#version 330

in vec3 normal;

out vec4 finalColor;

uniform vec3 color;

void main()
{
	float light = 0.75 + 0.18 * normal.y + 0.07 * normal.x;
	finalColor = vec4(color * light, 1.0);
}