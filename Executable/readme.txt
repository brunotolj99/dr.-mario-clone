
How to play:

Eliminate every virus before your opponent by matching 4 or more colors in the same row/column.

----------------

Controls:

Player 1
--------
Move Left/Right - A / D
Fall faster - S
Rotate Left/Right - N / M


Player 2
--------
Move Left/Right - Arrow Left / Arrow Right
Fall faster - Arrow Down
Rotate Left/Right - Numpad 1 / Numpad 2

----------------

Press ESC to exit the game